﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Calculator
{
    public class StringCalculator
    {
        public int Add(string numbers)
        {
            if(String.IsNullOrEmpty(numbers)) return 0;
             
            return Sum(numbers);
        }

        public int Sum(String numbers)
        {  
            int sum = 0; 
            string[] numbersList = SplitDelimiteredStringNumbers(numbers);
            int[] numbersArray = Array.ConvertAll(numbersList, int.Parse); 
            var negativeNumbers = new List<int>();

            foreach(int number in numbersArray.Where(value => value <= 1000))
            {
                if (number < 0)
                {
                    negativeNumbers.Add(number);
                }
                sum += number;
            }

            if (negativeNumbers.Any())
            {
               throw new Exception("Negatives not allowed: " + string.Join(",", negativeNumbers));
            }

            return sum;
        }

        public string[] SplitDelimiteredStringNumbers(string numbers)
        {
            char[] splitter = {',','\n'};
            return numbers.StartsWith("//") ? 
            cleanStringNumbersWithDefaultDelimeters(numbers) 
            : numbers.Split(splitter);
        }

        public string[] cleanStringNumbersWithDefaultDelimeters(string numbers)
        {
            string[] SplittedString;
            if (numbers[2] == '[')
            {
                int delimitersEndIndex = numbers.LastIndexOf(']');
                SplittedString = numbers.Substring(3, delimitersEndIndex - 3)
                        .Split(new[] { "][" }, StringSplitOptions.None);
                numbers = numbers.Substring(delimitersEndIndex + 1);
                numbers = numbers.TrimStart('\r', '\n');
                return SplittedString = numbers.Split(SplittedString, StringSplitOptions.None);
            }
            else
            {
               var defaultDelimeter = numbers.Substring(0, (numbers.IndexOf('\n')));
               var stringNumbers = numbers.Substring((numbers.IndexOf('\n')), numbers.Length-defaultDelimeter.Length);
               var customDelimeter = defaultDelimeter.Substring(2, defaultDelimeter.Length-2);
               stringNumbers = stringNumbers.TrimStart('\r', '\n');
               return SplittedString = stringNumbers.Split(customDelimeter);
            }
        }
    }
}
