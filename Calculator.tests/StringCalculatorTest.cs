using System;
using NUnit.Framework;

namespace Calculator.tests
{
    public class StringCalculatorTest
    {

        [Test]
        public void GIVEN_NullOrEmptyString_WHEN_Adding_RETURNS_Zero()
        {
            //arrange
            var calculator = new StringCalculator();
            int expectedResult = 0;
            string numbers = null;

            //act
            int actualResult = calculator.Add(numbers);

            //assert
            Assert.AreEqual(expectedResult, actualResult);
        }

        [Test]
        public void GIVEN_TwoNumbersSeparatedByComma_WHEN_Adding_RETURNS_TheSum()
        {
            //arrange
            var calculator = new StringCalculator();
            int expectedResult = 3;
            string numbers = "1,2";

            //act
            int actualResult = calculator.Add(numbers);

            //assert
            Assert.AreEqual(expectedResult, actualResult);
        }

        [Test]
        public void GIVEN_UnknownAmountOfNumbersSeparetedByComma_WHEN_Adding_RETURNS_TheSum()
        {
            //arrange
            var calculator = new StringCalculator();
            int expectedResult = 15;
            string numbers = "1,2,3,7,2";

            //act
            int actualResult = calculator.Add(numbers);

            //assert
            Assert.AreEqual(expectedResult, actualResult);
        }

        [Test]
        public void GIVEN_NumbersSeparatedByNewlinesAndCommas_WHEN_Adding_RETURNS_TheSum()
        {
            //arrange
            var calculator = new StringCalculator();
            int expectedResult = 21;
            string numbers = "1\n2,3\n7\n8";

            //act
            int actualResult = calculator.Add(numbers);

            //assert
            Assert.AreEqual(expectedResult, actualResult);
        }

        [Test]
        public void GIVEN_NumbersWithDefaultDelimeter_WHEN_Adding_RETURNS_TheSum()
        {
            //arrange
            var calculator = new StringCalculator();
            int expectedResult = 3;
            string numbers = "//;\n1;2";

            //act
            int actualResult = calculator.Add(numbers);

            //assert
            Assert.AreEqual(expectedResult, actualResult);
        }

        [Test]
        public void GIVEN_NumbersWithDelimetersOfAnyLength_WHEN_Adding_RETURNS_TheSum()
        {
            //arrange
            var calculator = new StringCalculator();
            int expectedResult = 6;
            string numbers = "//***\n1***2***3";

            //act
            int actualResult = calculator.Add(numbers);

            //assert
            Assert.AreEqual(expectedResult, actualResult);
        }

        [Test]
        public void GIVEN_NumbersWithMultipleDelimeters_WHEN_Adding_RETURNS_TheSum()
        {
           //arrange
            var calculator = new StringCalculator();
            int expectedResult = 6;
            string numbers = "//[*][%]\n1*2%3";

            //act
            int actualResult = calculator.Add(numbers);

            //assert
            Assert.AreEqual(expectedResult, actualResult);  
        }

        [Test]
        public void GIVEN_NumbersWithMultipleDelimetersOfAnyLength_WHEN_Adding_RETURNS_TheSum()
        {
           //arrange
            var calculator = new StringCalculator();
            int expectedResult = 7;
            string numbers = "//[****][%%%]\n2****2%%%3";

            //act
            int actualResult = calculator.Add(numbers);

            //assert
            Assert.AreEqual(expectedResult, actualResult);  
        }

        [Test]
        public void GIVEN_StringWithNegativeNumbers_WHEN_Adding_SHOULD_ReturnExceptionMessageAndNegativeNumbers()
        {
            //arrange
            var calculator = new StringCalculator();
            string expectedResult = "Negatives not allowed: " + "-3,-8";
            string numbers = "1\n2,-3\n7\n-8";

            //act
            var actualResult = Assert.Throws<Exception>(() => calculator.Add(numbers));

            //assert
            Assert.AreEqual(expectedResult, actualResult.Message);
        }

        [Test]
        public void GIVEN_NumbersOver1000_WHEN_Adding_RETURNS_TheSumWithoutNumbersOver1000()
        {
            //arrange
            var calculator = new StringCalculator();
            int expectedResult = 4;
            string numbers = "//***\n1***1001***3";

            //act
            int actualResult = calculator.Add(numbers);

            //assert
            Assert.AreEqual(expectedResult, actualResult); 
        }

        [Test]
        public void GIVEN_StringNumbersWithDelimiters_WHEN_Splitting_RETURNS_SplittedStringNumbersArray()
        {
            //arrange
            var calculator = new StringCalculator();
            string[] expectedResult = {"1","4","3"};
            string numbers = "//;\n1;4;3";

            //act
            string[] actualResult = calculator.SplitDelimiteredStringNumbers(numbers);

            //assert
            Assert.AreEqual(expectedResult, actualResult); 
        }

        [Test]
        public void GIVEN_StringNumbersWithMultipleDelimiters_WHEN_Splitting_RETURNS_StringNumbersArray()
        {
            //arrange
            var calculator = new StringCalculator();
            string[] expectedResult = {"1","3","3"};
            string numbers = "//[*][%]\n1*3%3";

            //act
            string[] actualResult = calculator.SplitDelimiteredStringNumbers(numbers);

            //assert
            Assert.AreEqual(expectedResult, actualResult); 
        }

        [Test]
        public void GIVEN_StringNumbers_WHEN_PassedToSumMethod_RETURNS_TheSum()
        {
            //arrange
            var calculator = new StringCalculator();
            int expectedResult = 7;
            string numbers = "//[*][%]\n1*3%3";

            //act
            int actualResult = calculator.Sum(numbers);

            //assert
            Assert.AreEqual(expectedResult, actualResult); 
        }
    }
}